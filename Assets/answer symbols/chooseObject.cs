﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chooseObject : MonoBehaviour {
	
	public AudioSource[] sounds;
	public AudioSource temperature_sound;
	public AudioSource answer_sound;
	

	public float semitone_offset = 0;
	// Use this for initialization
	void Start () {
		sounds = GetComponents<AudioSource>();
		temperature_sound = sounds[0];
		answer_sound = sounds[1];
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	
	void OnMouseOver() 
	{
		if (Input.GetMouseButtonDown(1)) {
			//PlayNote();
			answer_sound.Play();
		}
	}
	
	void OnCollisionEnter() {
		PlayNote();
	}
	void PlayNote() {
		AudioSource audio = GetComponent<AudioSource>();
		//audio = GetComponent<AudioSource>();
		GetComponent<AudioSource>().pitch = Mathf.Pow(2f,semitone_offset/12.0f);
		GetComponent<AudioSource>().Play ();
	}
}


