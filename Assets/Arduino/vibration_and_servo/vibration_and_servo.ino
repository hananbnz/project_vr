#include <VarSpeedServo.h> 
//#include <Servo.h>
// create servo objects
//Servo myservo;
VarSpeedServo myservo;
//VarSpeedServo myservo2;
int ratio = 51; // = 255/5 check if it's ok to be int and not float
 int pos = 20;
boolean newData = false;
const byte numChars = 5; // how many charachtars do I expect to recieve - 15 vibratos plus the precursor on the finger plus the end marker
//TODO: check if 5 is ok
char receivedChars[numChars]; // an array to store the received data
//char servo_symbol = 's';
//char vibrate_symbol = 'v';
 
void setup() {
  Serial.begin(9600);
  myservo.attach(8);    //For servomotor.
  pinMode( 6 , OUTPUT);  // For vibration. Must be a PWM pin

 
//  myservo2.attach(8);
} 
 
void loop() {
  static byte ndx = 0;
  char endMarker = '!';
  char rc;
  bool flagVibrate = false;
  bool flagServo = false;

if (Serial.available() > 0) 
  {
//    Serial.println("serial available begin");
    while (Serial.available() > 0 && newData == false) 
    {

        rc = Serial.read();
        if (rc != endMarker) 
        {
          
          receivedChars[ndx] = rc;
          ndx++;
          if (ndx >= numChars)
          {
          ndx = numChars - 1;
          }
            
        }
        else 
        {
            receivedChars[ndx] = '\0'; // terminate the string
            String message = "string was: ";
            String outputMsg = message + receivedChars;
//            Serial.println(outputMsg);
            ndx = 0;
            newData = true;
//           String outputString = String(receivedChars);  
//            if (outputString == String("0") )
//           {
//            
////              Serial.println("initial");
//             myservo.write(0, 0, true);
//            myservo.stop();
//            delay(1000);                        
//
////              myservo.write(0, 90, true);
////              myservo.wait();
////              delay(2000);
//           }
        }
     }
     if (newData==true)
    {
      String outputString = String(receivedChars);  
      bool checkDigit = isDigit(outputString[0]);
      if (checkDigit )
      //string's first char is a digit- write data to servotmotor
      {
       float outputDistance = outputString.toFloat();
       if (outputDistance >= 0.15 && outputDistance <= 4)
       {
        //write to servo only when user is in the range of possible distances
        //TODO: change degree to 0 in beginning of connect, and then go to pos and back
        //to 0 after clicking
          float delayServoF = map(outputDistance, 0.15, 4, 100, 1000);
          int delayServoWrite = (int)delayServoF;
          int id;
        for(id = 0; id <= 1 ; id += 1) // goes from 0 degrees to 180 degrees 
        {
          myservo.write(35, 0, true);
          myservo.stop();
          delay(delayServoWrite);                        
          myservo.write(0, 0, true);
          myservo.stop();
          delay(delayServoWrite);
        }

  
       }
       flagServo = false;
       
      }

      if (checkDigit == false)
      //string's first char isn't a digit- write data to vibration motor
      {
        if(outputString == "c2")
      {
      analogWrite( 6 , ratio*1.5 );  // % of duty cycle
      
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(1.5, DEC);
      delay(500);             // wait for 0.5s
      }
      
      if(outputString == "e2")
      {
      analogWrite( 6 , ratio*1.73 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(1.73, DEC);
       delay(500);             // wait for 0.5s
      }

      if(outputString == "g2")
      {
      analogWrite( 6 , ratio*1.96 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(1.96, DEC);
    delay(500);             // wait for 0.5s
      }


      if(outputString == "b2")
      {
      analogWrite( 6 , ratio*2.19 );  // % of duty cycle
        delay(500);              // play for 0.5s

      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(2.19, DEC);
       delay(500);             // wait for 4s
//      analogWrite( 6 , ratio*2.19 );  // % of duty cycle
      
      }

      if(outputString == "d3")
      {
      analogWrite( 6 , ratio*2.42 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(2.42, DEC);
       delay(500);             // wait for 0.5s
      
      }

      if(outputString == "f3")
      {
      analogWrite( 6 , ratio*2.65 );  // % of duty cycle
      delay(500);    
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(2.65, DEC);
     delay(500);             // wait for 0.5
      }

      if(outputString == "a3")
      {
      analogWrite( 6 , ratio*2.88 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(2.88, DEC);
       delay(500);             // wait for 0.5s
      
      }

      if(outputString == "c4")
      {
      analogWrite( 6 , ratio*3.11 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(3.11, DEC);
       delay(500);             // wait for 0.5s
      
      }

      if(outputString == "e4")
      {
      analogWrite( 6 , ratio*3.35 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(3.35, DEC);
       delay(500);             // wait for 0.5s
      
      }

      if(outputString == "g4")
      {
//      analogWrite( 6 , ratio*3.58 );  // % of duty cycle
      analogWrite( 6 , ratio*3.55 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(3.58, DEC);
      delay(100);  
//       delay(500);             // wait for 0.5s
      
      }

      if(outputString == "b4")
      {
      analogWrite( 6 , ratio*3.81 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
      Serial.println(3.81, DEC);
       delay(500);             // wait for 0.5s
      
      }

      if(outputString == "d5")
      {

      analogWrite( 6 , ratio*3.95 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
//      delay(20);             // wait for 0.5s
       Serial.println(3.95, DEC);
       delay(500);
       
     
      }

      if(outputString == "f5")
      {
      analogWrite( 6 , ratio*4.15 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
       delay(500);             // wait for 0.5s
      Serial.println(4.15, DEC);
      }

      if(outputString == "a5")
      {
      analogWrite( 6 , ratio*4.3 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
       delay(500);             // wait for 0.5s
      Serial.println(4.20, DEC);
      }


      if(outputString == "c6")
      {
      analogWrite( 6 , ratio*4.50 );  // % of duty cycle
      delay(500);              // play for 0.5s
      analogWrite( 6 , 0 );    // 0% duty cycle (off)
       delay(500);             // wait for 0.5s
      Serial.println(4.50, DEC);
      }

      flagVibrate = false;
      }
     
     

    newData = false;
   }
  

}

}


float mapf(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}




