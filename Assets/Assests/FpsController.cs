﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

    public float speed = 2f;
    CharacterController Player;
    float moveFB;
    float moveRL;
    
	// Use this for initialization
	void Start () {

        Player = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        moveFB = Input.GetAxis("Vertical") * speed;
        moveRL = Input.GetAxis("Horizontal") * speed;

        Vector3 movement = new Vector3(moveRL, 0 , moveFB);
        movement = transform.rotation * movement;
        Player.Move(movement * Time.deltaTime);
	}
}
