﻿using System.Collections;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// VR control is a class for managing the interaction between the player and the different objects in the game.
/// The interaction is implemented with a Raycast that point from the player POV (camera POV) to a 
/// define distance in the game with the parameter "distanceToSee".
/// The different objects in the game work with the relevant scripts attached to them, and can belong to one or both of the interfaces:
/// 1. Objects with feedback to the player - selecting the object with different controlers apply feedback to the user.
/// 2. Objects for game process - selecting the objects does something behind the scenes of the game,
///    mission counter and enabling the next mission.
/// </summary>
public class VRControl : MonoBehaviour {

    public float distanceToSee; // The max distance the player need to be in order to choose an object.
    RaycastHit whatIHit; // The Raycast will point to where the player is looking at the game.
    public AudioClick audioClick; // The script AudioClick
	public MainMenu mainMenue_script; // The script MainMenu
	public MissionManager MissionManager; // The script for making Shakshuka
	public CookingMission CookingMission; // The script for the cooking mission
	private static bool startPizzaMission = false; // initializer if Pizza mission is completed
	private static bool startDrinksMission = false; // initializer if Drink mission is completed
	public GameObject Manager_intro_1; // Game intro object
	public GameObject Manager_intro_2; // Game intro object
	public GameObject Manager_intro_3; // Game intro object
	public float def_intro_play_sec = 5f; // default time for game intro

	/* for log files - Remove after finish Main menue scene for advanced VR */
	const string seperator_msg = "\r\n ############################################################## \r\n";
	static string logFile = "";
	static int eventCounter = 0;
	static string currentLogFile = "";
	static string logDirectory = "";
	public bool ToMakeLog = true;

    // Use this for initialization
    void Start ()
    {
		if (ToMakeLog) {
			createRandomPlayer ();
		} else {
			PlayerPrefs.SetString("LogFileName", "");
		}
		// In the begining of the game will play the instruction on screen.
		play_intro();
    }
		

	// 
	/// <summary>
	/// Update is called once per frame for the game process
	/// </summary>
	void Update ()
    {
		control_raycast ();
    }

	/// <summary>
	/// This function controls the raycast - where the player is looking in the game.
	/// According to the game object group the player is looking at, the function will call the correct function for that object.
	/// The player will start with the "Drink" and "Pizza" missions and will be able the play them only after reading the relevant
	/// instruction in the game.
	/// After succeeding both "Drink" and "Pizza" missions the player can start the "Making shakshuka" mission.
	/// </summary>
	void control_raycast()
	{
		//Debug.DrawRay(this.transform.position, this.transform.forward * distanceToSee, Color.green);

		//cast a ray from the spawnpoint in the direction of its forward vector
		if (Physics.Raycast(this.transform.position, this.transform.forward, out whatIHit, distanceToSee))
		{
			// if selected Drink mission instruction
			if (whatIHit.collider.tag == "drinkInstruction") 
			{
				if (OVRInput.GetDown (OVRInput.Button.One)) {
					whatIHit.collider.gameObject.GetComponent<AudioClick> ().choose_obj ();
					if (whatIHit.collider.gameObject.name == "drink_cold") 
					{
						whatIHit.collider.gameObject.GetComponent<MissionManager> ().start_cooking_mission (whatIHit.collider.gameObject);
					}
					whatIHit.collider.tag = null;
					startDrinksMission = true;
				}
			// if selected Pizza mission instruction
			}else if (whatIHit.collider.tag == "pizzaInstruction") 
			{
				if (OVRInput.GetDown (OVRInput.Button.One)) {
					whatIHit.collider.gameObject.GetComponent<AudioClick> ().choose_obj ();
					if (whatIHit.collider.gameObject.name == "drink_cold") 
					{
						whatIHit.collider.gameObject.GetComponent<MissionManager> ().start_cooking_mission (whatIHit.collider.gameObject);
					}
					whatIHit.collider.tag = null;
					startPizzaMission = true;
				}
			}
			// The user can play the drink and pizza missions
			if (startDrinksMission || startPizzaMission) 
			{
				if (startDrinksMission && whatIHit.collider.tag == "drinkObjects") {

					if (OVRInput.GetDown (OVRInput.Button.One)) {
						whatIHit.collider.gameObject.GetComponent<AudioClick> ().choose_obj ();
						if (whatIHit.collider.gameObject.name == "drink_cold") 
						{
							whatIHit.collider.gameObject.GetComponent<MissionManager> ().start_cooking_mission (whatIHit.collider.gameObject);
						}
						whatIHit.collider.tag = null;


					} else if (OVRInput.GetDown (OVRInput.Button.Two)) {
						whatIHit.collider.gameObject.GetComponent<AudioClick> ().PlayNote (whatIHit.collider.gameObject);
						whatIHit.collider.tag = null;
					}
				}
				else if (startPizzaMission && whatIHit.collider.tag == "pizzaObjects") {

					if (OVRInput.GetDown (OVRInput.Button.One)) {
						whatIHit.collider.gameObject.GetComponent<AudioClick> ().choose_obj ();
						if (whatIHit.collider.gameObject.name == "pizza_second_hottest") 
						{
							whatIHit.collider.gameObject.GetComponent<MissionManager> ().start_cooking_mission (whatIHit.collider.gameObject);
						}
						whatIHit.collider.tag = null;


					} else if (OVRInput.GetDown (OVRInput.Button.Two)) {
						//Debug.Log ("player play note to: " + whatIHit.collider.gameObject.name);
						whatIHit.collider.gameObject.GetComponent<AudioClick> ().PlayNote (whatIHit.collider.gameObject);
						whatIHit.collider.tag = null;
					}
				}
			}
			// After the player finished the two tutorial mission (drink and pizza) he can continue to the next mission
			if (startPizzaMission && startDrinksMission) 
			{
				// TV selection is the instruction of the cooking mission
				if (whatIHit.collider.tag == "TV") 
				{
					if (OVRInput.GetDown (OVRInput.Button.One)) 
					{
						whatIHit.collider.gameObject.GetComponent<MissionManager> ().choose_tv();
						whatIHit.collider.tag = null;
					}

				}else if (whatIHit.collider.tag == "cookingMission") 
				{
					if (OVRInput.GetDown (OVRInput.Button.One)) 
					{
						whatIHit.collider.gameObject.GetComponent<CookingMission> ().cooking(whatIHit.collider.gameObject);
						whatIHit.collider.tag = null;
					}

				}
				else if (whatIHit.collider.tag == "PAN") 
				{
					if (OVRInput.GetDown (OVRInput.Button.One)) {
						whatIHit.collider.gameObject.GetComponent<CookingMission> ().cooking (whatIHit.collider.gameObject);
					} else if (OVRInput.GetDown (OVRInput.Button.Two)) 
					{
						whatIHit.collider.gameObject.GetComponent<AudioClick> ().PlayNote (whatIHit.collider.gameObject);
					}

					whatIHit.collider.tag = null;
				}
			}
		} 

	}
	// will Late call (delay) the function operation for "intro_play_sec".
	IEnumerator LateCall(GameObject Manager, float intro_play_sec)
	{
		yield return new WaitForSeconds(intro_play_sec);
		Manager.SetActive(false);
	}

	/// <summary>
	/// Plays an instruction intro when the game starts.
	/// can use the constant "def_intro_play_sec" to change the time the instructions will appear in the begining of the game.
	/// </summary>
	void play_intro()
	{
		Manager_intro_3.SetActive (true);
		Manager_intro_2.SetActive (true);
		Manager_intro_1.SetActive (true);
		StartCoroutine(LateCall(Manager_intro_1, 2 * def_intro_play_sec ));
		StartCoroutine(LateCall(Manager_intro_2, 3 * def_intro_play_sec + def_intro_play_sec));
		StartCoroutine(LateCall(Manager_intro_3, 4 * def_intro_play_sec + def_intro_play_sec));
	}
		
	/// <summary>
	/// Creates the random player Function will initialize the log data for a player without putting a name in, 
	/// so it will save the game logs to a "random player".
	/// </summary>
	private void createRandomPlayer()
	{
		string cuurentDir = Directory.GetCurrentDirectory();
		string current_date = DateTime.Now.ToString("d_M_yyyy");
		string currnt_time = DateTime.Now.ToString ();
		var logDirectory = cuurentDir + "\\logFiles";
		// if log directory doesn't exists will create it
		Directory.CreateDirectory(logDirectory);
		currentLogFile = logDirectory + "\\" + "random player" + " _ON_" + current_date + ".txt";
		string msg = seperator_msg + "EVENT " + eventCounter + " : player: " + "random player" + " is playing on " + current_date + "\r\n";
		if (!File.Exists(currentLogFile))
		{
			File.WriteAllText(Path.Combine(logDirectory, "random player" + " _ON_" + current_date + ".txt"), msg);
		}
		else
		{
			using (System.IO.StreamWriter file =
				new System.IO.StreamWriter(@currentLogFile, true))
			{
				file.WriteLine(msg);  // log_message
				file.Close();
			}
		}

		PlayerPrefs.SetString("LogFileName", currentLogFile);
		eventCounter++;
		PlayerPrefs.SetInt("eventCounter", eventCounter);
	}
}
