﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
public class FPS_Controller : MonoBehaviour
{
    //public static Game current;

    public float speed = 5f;

    // Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float straffe = Input.GetAxis("Horizontal") * speed;

        translation *= Time.deltaTime;
        straffe *= Time.deltaTime;

        transform.Translate(straffe, 0, translation);

        if (Input.GetKeyDown("escape"))
            Cursor.lockState = CursorLockMode.None;

        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            if (gameObject.GetComponent("AudioClick") as AudioClick != null)
            {
                Debug.Log("going to choose_obj");
                //choose_obj();
            }
        }
    }
}
