﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Log file writer class will initialize the text log file in the beginning of every game.
/// If the user insert a player name so the log file name will be the player name and the date of the game. 
/// Otherwise the name of the file will be "random player" and the date of the game.
/// Adding more data to the log file will be done in the relevant scripts in the game.
/// </summary>
public class LogFileWriter : MonoBehaviour
{

    const string seperator_msg = "\r\n ############################################################## \r\n";
    static string currentLogFile = "";
    static string logDirectory = "";
    static string playerName;
    static int eventCounter = 0;


	/// <summary>
	/// Will be called in the begining of the game for the user to insert the player name.
	/// </summary>
    void Start()
    {
		playerName = "random player";
        Debug.Log("start here");
        var input = gameObject.GetComponent<InputField>();
        var se = new InputField.SubmitEvent();
        se.AddListener(SubmitName);
        if(input != null)
        {
            input.onEndEdit = se;
        }
        
    }
	/// <summary>
	/// Will submits the name of the player inserted by the user.
	/// </summary>
	/// <param name="arg0">Arg0.</param>
    void SubmitName(string arg0)
    {
        Debug.Log("NAME: " + arg0);
        DateTime now = DateTime.Now;
        Debug.Log(now.ToString());
        string current_date = DateTime.Now.ToString("d_M_yyyy");
        Debug.Log("current date: " + current_date);
        playerName = arg0;
        
        
    }

    /// <summary>
    /// will create a log file for every player (optional - a seperate log file for every date the player play)
    /// Will add the first line to the file - playerName: "player name"   date: "date" 
    /// </summary>
    public void createLogFile()
    {
        Debug.Log("player name: " + playerName);
        string current_date = DateTime.Now.ToString("d_M_yyyy");
        string cuurentDir = Directory.GetCurrentDirectory();
        Debug.Log("current dir: " + cuurentDir);
        var logDirectory = cuurentDir + "\\logFiles";
        // if log directory doesn't exists will create it
        Directory.CreateDirectory(logDirectory);
        Debug.Log("before saving: " + cuurentDir);
        currentLogFile = logDirectory + "\\" + playerName + " _ON_" + current_date + ".txt";
        string msg = seperator_msg + "EVENT " + eventCounter +  " : player: " + playerName + " is playing on " + current_date + "\r\n";
        if (!File.Exists(currentLogFile))
        {
            File.WriteAllText(Path.Combine(logDirectory, playerName + " _ON_" + current_date + ".txt"), msg);
        }
        else
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@currentLogFile, true))
            {
                file.WriteLine(msg);  // log_message
            }
        }
        eventCounter++;
        PlayerPrefs.SetInt("eventCounter", eventCounter);
        PlayerPrefs.SetString("LogFileName", currentLogFile);

    }	
}
