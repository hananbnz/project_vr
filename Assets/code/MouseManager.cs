﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour {

	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfo;
            bool didHit = Physics.Raycast(ray, out hitInfo, 500.0f);
            if(didHit)
            {
                Debug.Log(hitInfo.collider.name + "    " + hitInfo.point);
                Temperature tempScript = hitInfo.collider.GetComponent<Temperature>();
                if(tempScript)
                {
                    tempScript.makeSound();
                }
            }
            else
            {
                Debug.Log("Clicked on empty spot");
            }
        }
      
		
	}
}
