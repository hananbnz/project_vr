﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour {

	public void onPlay() {
		GetComponent<AudioSource>().pitch = 1f;
		GetComponent<AudioSource>().Play();
		}
		
}
