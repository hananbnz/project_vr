using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System.Threading;



/// <summary>
/// The class Unity to arduino is used for sending bytes of data from Unity to Arduino, when object was chosen to check its temperature.
// Data is of string format- begins with digit for data to be sent to servomotor,
// and begins with letter for data to be sent to the vibration motor. According to the first char in the string, it's decided in the arduino module
// to which pin should it apply the analogWrite.
/// </summary>

// IMPORTANT NOTE: 
// The connection to the arduino is done by the serial port "COM3", if it's different on your PC please change that.
// After the user exits the game, the connection closes.

public class UnityToArduino : MonoBehaviour {


	// Define the serial port for connection:
	public static SerialPort sp = new SerialPort("COM3", 9600);


	public string message2;
	float timePassed = 0.0f;
	static string outputString;
	static string endData = "!";
	public string initServoPos = "0";

	bool shouldExit = false;

	//public string audioFilename;
	// Use this for initialization
	void Start () {
		// Define a thread for connecting to Arduino - minizes the delay!
		Thread myThread = new Thread(new ThreadStart(ThreadWorker));
		myThread.Start();
		// Message for ferifying the serial port was opened
		print("opened from start");
	}

	// Update is called once per frame
	void Update () {
	}


	void ThreadWorker ()
	{
		//	Calls the opening of the connection to the serial port
		if(shouldExit == false) {
			OpenConnection();
		}

	}


	public void OpenConnection() 
	{
		// Connecting the serial port
		if (sp != null) 
		{
			if (sp.IsOpen) 
			{
				sp.Close();
				print("Closing port, because it was already open!");
			}
			else 
			{
				sp.Open();  // opens the connection
				sp.ReadTimeout = 50;	// The value of miliseconds to wait between calls
				print("Port Opened!");
				//Initializing the position of the servomotor- all movements will begin from this position
				sendDistanceToServo(ref initServoPos);	
			}
		}
		else 
		{
			if (sp.IsOpen)
			{
				print("Port is already open");
			}
			else 
			{
				print("Port == null");
			}
		}
	}

	void OnApplicationQuit() 
	{
		//Closing the connection when the game stops, so next time we play we'll reconnect
		print("closing port");
		sp.Close();
		shouldExit = true;
	}



	public static void sendVoltageVibration(ref string audioFilename){
		// This function sends the given string from AudioClick module, to the Arduino.
		// The info in the string is digits.

		outputString = audioFilename + endData;
		// Send the string to the arduino via the opened serial port
		sp.Write(outputString);

		//Below line is for reading from arduino, verifying the wanted value was written
		string arduinoOutput = ReadFromArduino();
	}


	public static void sendDistanceToServo(ref string distance_string){
		//byte[] MyMessage = System.Text.Encoding.UTF8.getBytes(audioFilename);
		//MySerialPort.Write(MyMessage,0,MyMessage.Length);	
		//OpenConnection();
		string outputStringDistance = distance_string + endData;
		print("sending from unity");
		sp.Write(outputStringDistance);
		//sp.Write(audioFilename);
		//int cur_timeout = 0;
		string arduinoOutput = ReadFromArduino();
		//sp.Write(outputString);
	}


	public static string ReadFromArduino () {
		// The purpose of this function is only for debugging and verifying the wanted value was written in 
		// the arudino pin.

		int timeout;
		try {
			string msg_from_arduino = sp.ReadLine();
			print(msg_from_arduino);
			return msg_from_arduino;
		}
		catch (System.TimeoutException e) {
			print("timeout caught");
			return null;
		}
	}



}