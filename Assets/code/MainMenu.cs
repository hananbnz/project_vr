﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.VR;

/// <summary>
/// The Main menu class is for the first screen of the game, here the user select the name of the player or play as a random player.
/// </summary>
public class MainMenu : MonoBehaviour {

	// TODO need to check for advanced VR
    const string seperator_msg = "\r\n ############################################################## \r\n";
    static string logFile = "";
    static int eventCounter = 0;
    static string currentLogFile = "";
    static string logDirectory = "";



    void Start()
    {
		UnityEngine.XR.XRSettings.enabled = false;
        logFile = "random";
    }

	/// <summary>
	/// Plaies the game as a random player.
	/// </summary>
    public void PlayGameRandom()

    {
        createRandomPlayer();
        SceneManager.LoadScene("Room Pro Interior");
    }

	/// <summary>
	/// Plaies the game as a specific player after inserting a name and initializing his log file.
	/// </summary>
    public void PlayGame()
    {

        Debug.Log("Play");
        SceneManager.LoadScene("Room Pro Interior");
    }

	/// <summary>
	/// Quits the game.
	/// </summary>
    public void QuitGame()
    {
        Debug.Log("Quit!!");
        Application.Quit();
    }
		
	/// <summary>
	/// Creates a random player and initialize a log file to output the player actions in the game.
	/// </summary>
    public void createRandomPlayer()
    {
        string cuurentDir = Directory.GetCurrentDirectory();
        string current_date = DateTime.Now.ToString("d_M_yyyy");
        var logDirectory = cuurentDir + "\\logFiles";
        // if log directory doesn't exists will create it
        Directory.CreateDirectory(logDirectory);
        currentLogFile = logDirectory + "\\" + "random player" + " _ON_" + current_date + ".txt";
        string msg = seperator_msg + "EVENT " + eventCounter + " : player: " + "random player" + " is playing on " + current_date + "\r\n";
        if (!File.Exists(currentLogFile))
        {
            File.WriteAllText(Path.Combine(logDirectory, "random player" + " _ON_" + current_date + ".txt"), msg);
        }
        else
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@currentLogFile, true))
            {
                file.WriteLine(msg);  // log_message
            }
        }
        PlayerPrefs.SetString("LogFileName", currentLogFile);
        eventCounter++;
        PlayerPrefs.SetInt("eventCounter", eventCounter);
    }

}
