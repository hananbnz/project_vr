﻿using System.Collections;
using System;
using System.IO;
using UnityEngine;

/// <summary>
/// The class Mission Manager is for manage and coordinate the different missions in the game -  
/// The "Drink" and "Pizza" missions and then the cooking mission.
/// </summary>
public class MissionManager : MonoBehaviour
{

    public GameObject Manager; // First manager parameter 
	public GameObject Manager1; // Second manager parameter 
    static string logFile = "";
    static int eventCounter = 0;
    private static string right_pizza = "pizza_second_hottest"; // correct answer for the pizza mission
	private static string right_drink = "drink_cold"; // correct answer for the drink mission
    // The integer parameters used for the mission counter 
	private static int pizza_mission_completed = 1;
    private static int drink_mission_completed = 2;
    private static int mission_counter = 0;
    private static int MISSION_TV_STAGE = 3;

	public float sec = 2.5f; //Time for the popup symbols to appear after selecting the objects
    
	// Use this for initialization
    void Start()
    {
        logFile = PlayerPrefs.GetString("LogFileName");
        eventCounter = PlayerPrefs.GetInt("eventCounter");
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("making shakshuka - now mission counter is: " + mission_counter);
        if (mission_counter == MISSION_TV_STAGE)
        {
            //manager2.SetActive(true);
        }

    }

	// TODO the mission counter and TV selection to the old non-VR version, left here if will be needed
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (gameObject.name == right_drink)
            {
                set_mission_counter(drink_mission_completed);
                //Debug.Log("drink_mission_completed");
            }
            else if (gameObject.name == right_pizza)
            {
                set_mission_counter(pizza_mission_completed);
                //Debug.Log("pizza_mission_completed");

            }

            if (mission_counter == MISSION_TV_STAGE)
            {
                if(gameObject.name == "Tv Note cooking mission")
                {
                    Manager.SetActive(true);
                    Manager1.SetActive(true);
                    Invoke("Deactivate1", 2);
                }
            }   
        }
    }

	void Deactivate1()
	{
		Manager1.SetActive(false);
	}
	/// <summary>
	/// set the parameter of the missions counter.
	/// </summary>
	/// <param name="current_object"> Current object.</param>
	public void start_cooking_mission(GameObject current_object)
	{
		if (current_object.name == right_drink)
		{
			set_mission_counter(drink_mission_completed);
		}
		else if (current_object.name == right_pizza)
		{
			set_mission_counter(pizza_mission_completed);
		}
	
	}

	// Will make the popup disappear after 'sec' number of seconds.
	IEnumerator LateCall(GameObject Manager)
	{
		yield return new WaitForSeconds(sec);
		Manager.SetActive(false);
	}

	/// <summary>
	/// This function will popup the instruction for the cooking mission after selecting the TV object.
	/// </summary>
	public void choose_tv()
	{
		if (mission_counter == MISSION_TV_STAGE) 
		{
			Manager.SetActive(true);
			Manager1.SetActive(true);
			StartCoroutine(LateCall(Manager1));
		}

	}

	/// <summary>
	/// Will Set the mission counter.
	/// </summary>
	/// <param name="param">Parameter.</param>
    void set_mission_counter(int param)
    {
        if (mission_counter == 0)
        {
            mission_counter += param;
        }
        else if (mission_counter != param && mission_counter < MISSION_TV_STAGE)
        {
            mission_counter += param;
        }
        PlayerPrefs.SetInt("game_mission_counter", mission_counter);
    }

}

