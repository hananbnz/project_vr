﻿using System.Collections;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;


/// <summary>
/// The class Audio click connects between an object in the game and the different outputs the game have for it.
/// For every object the class manage the sound and vibration output according to the temperature and the tapping
/// output according to the distance of the player from the selected object.
/// </summary>
public class AudioClick : MonoBehaviour {

	public GameObject character;
	public GameObject pan;
	public GameObject temperatureObject;
	public float semitone_offset = 0;
	public GameObject Manager;
	public GameObject Manager1;
	private float timerStart = 0f;
	public string audio_name;
	private float timerEnd = 2f;
	static string logFile = "";
	string curPotTemperature = "20";
	static int actualTemp = 20;
	static int eventCounter = 0;
	public float output_voltage = 0;
	public float sec = 2.5f;
	private static bool startPizzaMission;
	private static bool startDrinksMission;
	public activateSoundObject soundManager;




	// Use this for initialization
	void Start ()
	{
		logFile = PlayerPrefs.GetString("LogFileName");
		curPotTemperature = PlayerPrefs.GetInt("actualTemp", actualTemp).ToString();
		startPizzaMission = false;
		startDrinksMission = false;
	}
		
	IEnumerator LateCall(GameObject Manager)
	{
		yield return new WaitForSeconds(sec);
		Manager.SetActive(false);
	}

	// TODO - only for non VR version 
	void OnMouseDown() {

		PlayNote(gameObject);
	}

	// TODO - only for non VR version
	void OnMouseOver() 
	{
		if (Input.GetMouseButtonDown(1)) {
			choose_obj();       
		}
	}

	/// <summary>
	/// Checks the mission validity so player can do a mission in game only after finding the relevant instructions.
	/// </summary>
	/// <returns><c>true</c>, if mission validity was checked, <c>false</c> otherwise.</returns>
	/// <param name="current_object">Current object.</param>
	bool check_mission_validity(GameObject current_object)
	{
		if (current_object.name == "pizza_note") {
			startPizzaMission = true;
			return true;
		} else if (current_object.name.Contains ("Pizza")) {
			if (startPizzaMission) {
				return true;
			}
			return false;

		} else if (current_object.name == "Wine_Bottle") {
			startDrinksMission = true;
			return true;
		} else if (current_object.name.Contains ("drink")) {
			if (startDrinksMission) {
				return true;
			}
			return false;
		} else {
			return true;
		}

	}

	// TODO - only for non VR version deals the popup in game when player choose an object
	public void choose_obj()
	{
		Manager.SetActive(true);
		StartCoroutine(LateCall(Manager));
		if (logFile != "")
		{
			appendLogFile("selected " + gameObject.name + " at " + DateTime.Now.ToString());
		}
		if (Manager1 != null)
		{
			Manager1.SetActive(true);
			StartCoroutine(LateCall(Manager1));
		}

		if (Manager1 != null)
		{
			Manager1.SetActive(true);
			StartCoroutine(LateCall(Manager1));
		}  
	}

	// TODO - only for non VR version, play the sound for the selected object
	void OnCollisionEnter() {
		PlayNote(gameObject);
	}


	/// <summary>
	/// This function plays the note, and sends the information of the current note to the module UnityToArduino as string,
	/// in wanted format, so the vibration will be activated. The information of the distance will be sent also as a string,
	/// in wanted format, so the vibration will be activated. The information of the distance will be sent also as a string,
	/// for the servomotor.
	/// </summary>
	/// <param name="current_object">Current object.</param>
	public void PlayNote(GameObject current_object)
	{
		AudioSource audio = GetComponent<AudioSource>();
		if (audio != null)
		{
			AudioClip cur_clip = audio.clip;
			if (cur_clip != null)
			{
				//Getting the audio name:
				audio_name = cur_clip.name;

				// Send the information of the current note for the wanted vibration
				UnityToArduino.sendVoltageVibration(ref audio_name);

				character = GameObject.Find("CenterEyeAnchor");
				Vector3 character_pos = character.transform.position;
				var idist = Vector3.Distance(transform.position, character_pos);// Get distance between the cam and the player
				string outputDistanceString = idist.ToString("0.00"); //2 decimal places accuracy

				// Send the information of the distance from user to selected object
				UnityToArduino.sendDistanceToServo(ref outputDistanceString);

				GetComponent<AudioSource>().pitch = Mathf.Pow(2f, semitone_offset / 12.0f);
				GetComponent<AudioSource>().Play();
				if (logFile != "") 
				{
					appendLogFile("check sound of " + gameObject.name + " at " + DateTime.Now.ToString());	
				}

			}
		}
			
		//if (object_name.CompareTo("shakshuka pot") == 0)
		if (current_object.tag == "PAN")
			//TODO: change name above to "shakshuka pot" 

		{
			//int actualTemp; 
			curPotTemperature = PlayerPrefs.GetInt("actualTemp", actualTemp).ToString();
			temperatureObject = GameObject.Find(curPotTemperature);
			activateSoundObject soundManager = (activateSoundObject) temperatureObject.GetComponent(typeof(activateSoundObject));
			AudioSource potAudio = soundManager.getAudio();
			AudioClip cur_clip = potAudio.clip;
			string potAudioName = cur_clip.name;
			UnityToArduino.sendVoltageVibration(ref potAudioName);


			character = GameObject.Find("CenterEyeAnchor");
			Vector3 character_pos = character.transform.position;
			var idist = Vector3.Distance(transform.position, character_pos);// Get distance between the cam and the player
			string outputDistanceString = idist.ToString("0.00"); //2 decimal places accuracy

			UnityToArduino.sendDistanceToServo(ref outputDistanceString);
			//Play the according sound
			potAudio.Play();

			//TODO: add vibrations / servo to pot
			//activateSoundObject.PlayPotNote(ref curPotTemperature);
			//temperatureObject.SetActive(true);
			//temperatureObject.SetActive(false);
		}
	
	}

	/// <summary>
	/// Appends the input information from the player to the log file.
	/// </summary>
	/// <param name="log_message">Log message.</param>
	public void appendLogFile(string log_message)
	{
		//string currentLogFile = logDirectory + "\\" + playerName + " _ON_" + current_date + ".txt";
		PlayerPrefs.GetInt("eventCounter", eventCounter);
		using (System.IO.StreamWriter file =
			new System.IO.StreamWriter(@logFile, true))
		{
			file.WriteLine("EVENT " + eventCounter + " : " + log_message);  // log_message
			file.Close ();
		}
		eventCounter++;
		PlayerPrefs.SetInt("eventCounter", eventCounter);
	}
}