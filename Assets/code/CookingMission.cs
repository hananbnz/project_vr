﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;

/// <summary>
/// The Cooking mission calss contain all the logic for the cooking mission.
///  the class contain all the ingredients (objects) for the mission, the food, pot, and the temperature up and down buttuns.
/// The player need to put the ingredients in the pot in specific order where for every ingredient he need the pot
/// to have a specific temperature. If the user do a mistake about the required temperature in each step, 
/// the mission will start all over again.
/// </summary>
public class CookingMission : MonoBehaviour {

	// Game object and different parameters for the cooking mission
    public GameObject FinishCooking;
    public GameObject CorrectAnswer;
    public GameObject WrongAnswer;
    public GameObject madeOil;
    public GameObject madeOnion;
    public GameObject madesauce;
    private GameObject onion_obj;
    private GameObject egg1_obj;
    private GameObject egg2_obj;
    private GameObject tomatoSauce_obj;
    private GameObject oliveOil_obj;
	private static int actualTemp = 20;
	private static int MAX_TEMP = 120;
	private static int MIN_TEMP = 20;
	private static int OLIVE_OIL_TEMP = 20;
	private static int ONION_TEMP = 80;
	private static int TOMATO_SAUCE_TEMP = 110;
	private static int EGGS_TEMP = 120;
	private static int FINISH_TEMP = 90;
	private static string oliveOil = "oliveOilBottle";
	private static string egg1 = "egg1";
	private static string egg2 = "egg2";
	private static string onion = "Onion";
	private static string shakshukaPot = "Shakshuka Pot";
	private static string tomatosauce = "TomatoSauseCan";
	private static string lowerTemp = "tempLower";
	private static string raiseTemp = "tempHigher";
	private static bool isOliveOilIn = false;
	private static bool isOnionIn = false;
	private static bool isTometoIn = false;
	private static bool isegg1In = false;
	private static bool isegg2In = false;
	public static GameObject cur_temp_manager;
	private GameObject parent_object;

	// Log file parameters
    static string logFile = "";
    static int eventCounter = 0;

	// default time for pop up information in game.
	public float sec = 4f;


    // Use this for initialization
    void Start () {
        logFile = PlayerPrefs.GetString("LogFileName");
        eventCounter = PlayerPrefs.GetInt("eventCounter");
        onion_obj = GameObject.Find("Onion");
        oliveOil_obj = GameObject.Find("oliveOilBottle");
        egg1_obj = GameObject.Find("egg1");
        egg2_obj = GameObject.Find("egg2");
        tomatoSauce_obj = GameObject.Find("TomatoSauseCan");
		PlayerPrefs.SetInt("actualTemp", actualTemp);
		//cur_temp = GameObject.Find ("20_image");
		parent_object = GameObject.Find("Temperature_images");
    }
	
	// Update is called once per frame
	void Update () {
    }

	IEnumerator LateCall(GameObject curObj, GameObject otherObj)
	{
		yield return new WaitForSeconds(sec);
		curObj.SetActive(false);
		otherObj.SetActive (false);
	}
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
			cooking (gameObject);
        }     
    }

	/// <summary>
	/// Cooking the specified current_object.
	/// </summary>
	/// <param name="current_object">The current object the player select in game.</param>
	public void cooking(GameObject current_object)
	{
		// Changing the Temperature
		if (current_object.name == raiseTemp)
		{
			//appendLogFile("raised the temperature by 10 degrees " + " at " + DateTime.Now.ToString());
			changeTemp(10);
		}
		else if (current_object.name == lowerTemp)
		{
			//appendLogFile("lowered the temperature by 10 degrees " + " at " + DateTime.Now.ToString());
			changeTemp(-10);
		}
		else if (current_object.name == oliveOil)
		{

			if (actualTemp != OLIVE_OIL_TEMP)
			{
				restartkCookingMission();
			}
			else
			{
				isOliveOilIn = true;
				Deactivate(oliveOil_obj);
				madeOil.SetActive(true);
				if (logFile != "") {
					appendLogFile ("inserted olive oil at " + DateTime.Now.ToString ());
				}
			}
		}
		else if (isOliveOilIn && current_object.name == onion)
		{
			if (actualTemp != ONION_TEMP)
			{
				restartkCookingMission();
			}
			else
			{
				if (logFile != "") {
					appendLogFile ("inserted onion at " + DateTime.Now.ToString ());
				}
				Deactivate(onion_obj);
				Deactivate(madeOil);
				madeOnion.SetActive(true);
				isOnionIn = true;
			}
		}
		else if (isOnionIn && current_object.name == tomatosauce)
		{
			if (actualTemp != TOMATO_SAUCE_TEMP)
			{
				restartkCookingMission();
			}
			else
			{
				if (logFile != "") {
					appendLogFile ("inserted tomato sauce at " + DateTime.Now.ToString ());
				}
				Deactivate(tomatoSauce_obj);
				Deactivate(madeOnion);
				madesauce.SetActive(true);
				isTometoIn = true;
			}

		}
		else if (isTometoIn && current_object.name == egg1)
		{
			if (actualTemp != EGGS_TEMP)
			{
				restartkCookingMission();
			}
			else
			{
				Deactivate(egg1_obj);
				isegg1In = true;
			}

		}
		else if (isegg1In && current_object.name == egg2)
		{
			if (actualTemp != EGGS_TEMP)
			{
				restartkCookingMission();
			}
			else
			{
				if (logFile != "") {
					appendLogFile ("inserted eggs at " + DateTime.Now.ToString ());
				}
				Deactivate(egg2_obj);
				isegg2In = true;
			}

		}
		else if (isegg2In)
		{
			if (actualTemp == FINISH_TEMP)
			{
				// finish the mission - V and put made eggs
				FinishCooking.SetActive(true);
				CorrectAnswer.SetActive(true);
				Invoke("Deactivate_correct_answer", 2);
				if (logFile != "") {
					appendLogFile ("finished cooking at " + DateTime.Now.ToString ());
				}
			}
		}
	}

    void Deactivate(GameObject cur_obj)
    {
        // add the invoke after 2 seconds
        cur_obj.SetActive(false);
    }

    void Deactivate_wrong_answer()
    {
        WrongAnswer.SetActive(false);
    }

    void Deactivate_correct_answer()
    {
        CorrectAnswer.SetActive(false);
    }
		
	/// <summary>
	/// Restarts the cooking mission.
	/// </summary>
    void restartkCookingMission()
    {
		if (logFile != "") {
			appendLogFile ("wrong answer, restart cooking mission at " + DateTime.Now.ToString ());
		}
		string actual_temp = actualTemp.ToString () + "_image";
		foreach (Transform transform in parent_object.transform) {
			if (transform.gameObject.name == actual_temp) 
			{
				cur_temp_manager = transform.gameObject;
				break;
			}
		}
		cur_temp_manager.SetActive (true);
		WrongAnswer.SetActive(true);
		StartCoroutine(LateCall(WrongAnswer, cur_temp_manager));
        Deactivate(madeOil);
        Deactivate(madeOnion);
        Deactivate(madesauce);
        onion_obj.SetActive(true);
        egg1_obj.SetActive(true);
        egg2_obj.SetActive(true);
        tomatoSauce_obj.SetActive(true);
        oliveOil_obj.SetActive(true);
        actualTemp = 20;
		PlayerPrefs.SetInt("actualTemp", actualTemp);
        isOliveOilIn = false;
        isOnionIn = false;
        isTometoIn = false;
        isegg1In = false;
        isegg2In = false;        
    }

	/// <summary>
	/// Changes the temp of the pot on the stove in the game.
	/// </summary>
	/// <param name="tempEdit">Temp edit.</param>
    void changeTemp(int tempEdit)
    {
        int tmp = actualTemp + tempEdit;
        if (tmp >= MIN_TEMP && tmp <= MAX_TEMP)
        {
            actualTemp = tmp;
        }
		PlayerPrefs.SetInt("actualTemp", actualTemp);
    }

	/// <summary>
	/// Appends the input information from the player to the log file.
	/// </summary>
	/// <param name="log_message">Log message.</param>
    private void appendLogFile(string log_message)
    {
        //string currentLogFile = logDirectory + "\\" + playerName + " _ON_" + current_date + ".txt";
        eventCounter = PlayerPrefs.GetInt("eventCounter", eventCounter);
        using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@logFile, true))
        {
            file.WriteLine("EVENT " + eventCounter + " : " + log_message);  // log_message
			file.Close();
        }
        eventCounter++;
        PlayerPrefs.SetInt("eventCounter", eventCounter);
    }
    
}
