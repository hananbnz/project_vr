﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

/// <summary>
/// The Pause menu class is used for pausing the game with options to resume or quit the game at any time.
/// </summary>
public class PauseMenu : MonoBehaviour
{

    public Transform canvas;
    public Transform Player;
    static string logFile = "";
    static int eventCounter;

    private void Start()
    {
        logFile = PlayerPrefs.GetString("LogFileName");
    }

    // Update is called once per frame
    void Update()
    {
		// when the buttun Escape is down will pause the game, TODO maybe change for VR?
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }

    }
	/// <summary>
	/// This function will pause the game. Will write the pausing time as an event in the log file
	/// and stop the game for the time period the user will pause the game.
	/// </summary>
    public void Pause()
    {
        if (canvas.gameObject.activeInHierarchy == false)
        {
            eventCounter = PlayerPrefs.GetInt("eventCounter", eventCounter);
            appendLogFile("EVENT: "+ eventCounter + ": " + "player paused game at " + DateTime.Now.ToString());
            eventCounter++;
            PlayerPrefs.SetInt("eventCounter", eventCounter);
            canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
            Player.GetComponent<FPS_Controller>().speed = 0;
        }
        else
        {
            Resume();
        }
    }
	/// <summary>
	/// Resume will resume the game from the point the user paused it.
	/// </summary>
    public void Resume()
    {
        appendLogFile("EVENT: " + eventCounter + ": " + "player resumed game at " + DateTime.Now.ToString());
        canvas.gameObject.SetActive(false);
        Time.timeScale = 1;
		// TODO - need to change the "FPS_Controller" to the advanced VR player later.
        Player.GetComponent<FPS_Controller>().speed = 2;
    }

	/// <summary>
	/// Quit the game
	/// </summary>
    public void Quit()
    {
        appendLogFile("player Quit game at " + DateTime.Now.ToString());
        Application.Quit();
    }

	/// <summary>
	/// will add the information about pausing, resuming and quitting the game to the log file
	/// </summary>
	/// <param name="log_message"> the log message.</param>
    public void appendLogFile(string log_message)
    {
        eventCounter = PlayerPrefs.GetInt("eventCounter", eventCounter);
        using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@logFile, true))
        {
            file.WriteLine(log_message);
        }
        eventCounter++;
        PlayerPrefs.SetInt("eventCounter", eventCounter);
    }

}
